from django.contrib.auth.models import User


def retrieve_user_info(user_ids):
    return list(User.objects.filter(id__in=user_ids).
                values('id', 'first_name', 'last_name'))


