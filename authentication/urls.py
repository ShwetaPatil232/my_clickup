"""clickup URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .views import *


urlpatterns = [
    path('v1/login/', login_view),
    path('v1/logout/', logout_view),
    path('v1/signup/', signup_user_account),
    path('v1/verify_signup_otp/', verify_signup_otp_and_create_user),
    path('v1/forget_password/', forget_password),
    path('v1/reset_password/', reset_password),
    path('v1/otp_check/', otp_check),

]
