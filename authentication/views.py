import json
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import JsonResponse
from django.contrib.auth import login, logout
from django.views.decorators.http import require_http_methods
from django.core.mail import send_mail
from clickup.settings import EMAIL_HOST_USER
from .models import UserSignUp
import random
import datetime
from django.contrib.auth.decorators import login_required

# Create your views here.


@require_http_methods(["POST"])
def login_view(request):
    params = json.loads(request.body)
    queryset = User.objects.filter(
        username=params['username']
    )
    if queryset.exists():
        user = queryset.last()
        is_password_correct = user.check_password(params['password'])
        if is_password_correct:
            login(request, user)
            data = {
                "id": user.id,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "email": user.email,
                "last_login": user.last_login,
                "data_of_joining": user.date_joined,
            }
            return JsonResponse({
                "message": "Success",
                "status": True,
                "data": data
            })
        else:
            return JsonResponse({
                "message": "Incorrect Password",
                "status": False
            })
    else:
        return JsonResponse({
            "message": "User Not Found",
            "status": False
        })


@login_required
@require_http_methods(["POST"])
def logout_view(request):
    logout(request)
    return JsonResponse({
            "message": "Logout Successfully.",
            "status": True
        })


@require_http_methods(["POST"])
def signup_user_account(request):
    params = json.loads(request.body)
    otp = random.randrange(1111,9999)
    instance = UserSignUp.objects.create(
        username=params.get('username'),
        password=params.get('password'),
        first_name=params.get('first_name'),
        last_name=params.get('last_name'),
        email=params.get('email'),
        otp=otp,
        otp_created_time = datetime.datetime.now())
    message = f'Your account creation otp is {otp}.'
    send_mail(
        'ClickUP Account Creation',
        message,
        EMAIL_HOST_USER,
        [params.get('email')],
        fail_silently=False,)
    return JsonResponse({
        "message": "OTP Sent on email",
        "status": True,
        "data": instance.get_json()
    })


@require_http_methods(["POST"])
def verify_signup_otp_and_create_user(request):
    params = json.loads(request.body)
    _id = int(params.get('id'))
    input_otp = int(params.get('otp'))
    username = params.get('username')
    password = params.get('password')
    confirm_password = params.get('confirm_password')

    if not password == confirm_password:
        return JsonResponse({
            "message": "Both password not matched",
            "status": False,
        })
    queryset: UserSignUp = UserSignUp.objects.filter(id=_id, username=username)
    if queryset.exists():
        instance = queryset.last()
        if input_otp == instance.otp:
            user_instance: User = User.objects.create(username=instance.username)
            user_instance.set_password(password)
            user_instance.first_name = instance.first_name
            user_instance.last_name = instance.last_name
            user_instance.email = instance.email
            user_instance.save()

            data = {
                "id": user_instance.id,
                "username": user_instance.username,
                "first_name": user_instance.first_name,
                "last_name": user_instance.last_name,
                "email": user_instance.email,
                "last_login": user_instance.last_login,
                "data_of_joining": user_instance.date_joined,
            }
            return JsonResponse({
                "message": "Account created successfully",
                "status": True,
                "data": data
            })
        else:
            return JsonResponse({
                "message": "Invalid OTP",
                "status": False,
            })
    else:
        return JsonResponse({
            "message": "Detail not found",
            "status": False
        })


@require_http_methods(["POST"])
def otp_check(request):
    params = json.loads(request.body)
    email = params.get('email')
    otp = params.get('otp')

    queryset: UserSignUp = UserSignUp.objects.filter(email=email)
    if queryset.exists():
        instance = queryset.first()
        checktime = datetime.datetime.now()
        var1 = (checktime - instance.otp_created_time)
        if (var1.seconds // 60) < 5: #seconds compare
            if instance.otp == otp:
                return JsonResponse({
                    "message": "Valid OTP",
                    "status": True
                })
            else:
                return JsonResponse({
                    "message": "Invalid OTP",
                    "status": False
                })
        else:
            return JsonResponse({
                "message": "OTP Expired ",
                "OTP Creations Time : ": [instance.otp_created_time],
                "OTP Check Time : ": [checktime]
            })


@require_http_methods(["POST"])
def forget_password(request):
    params = json.loads(request.body)
    email = params.get('email')
    username = params.get('username')
    otp = random.randrange(1111,9999)

    UserSignUp.objects.filter(email=email).update(otp=otp, otp_created_time=datetime.datetime.now())
    instance = UserSignUp.objects.get(email=email)
    if instance is not None:
        message = f'Your account validation otp is {instance.otp}.'
        send_mail(
            'ClickUP Account Forget Password OTP',
            message,
            EMAIL_HOST_USER,
            [params.get('email')],
            fail_silently=False,
        )
        return JsonResponse({
            "message": "OTP Sent on email",
            "status": True,
            "data": [instance.get_json()]
        })
    else:
        return JsonResponse({
            "message": "User not found",
            "status": False
        })


@login_required
@require_http_methods(["POST"])
def reset_password(request):
    if not request.user.is_authenticated:
        return JsonResponse({
            "message": "Login First.",
            "status": False,
        })
    params = json.loads(request.body)
    email = params.get('email')
    new_password = params.get('new_password')
    confirm_password = params.get('confirm_password')

    if not new_password == confirm_password:
        return JsonResponse({
            "message": "Both password are not match",
            "status": False,
        })

    queryset: UserSignUp = UserSignUp.objects.filter(email=email)
    if queryset.exists():
        instance = queryset.last()
        instance.password = new_password
        instance.save()
        return JsonResponse({
            "message": "Password Reset successfully",
            "status": True,
            "data": [instance.get_json()]
        })
    else:
        return JsonResponse({
            "message": "User not found",
            "status": False
        })