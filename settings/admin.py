from django.contrib import admin
from .models import WorkSpace, Team


# Register your models here.


@admin.register(WorkSpace)
class WorkspaceAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ['id', 'workspace']