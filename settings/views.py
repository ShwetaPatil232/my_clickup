import json
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import JsonResponse
from django.db import models
from django.views.decorators.http import require_http_methods
from .models import WorkSpace, Team


@require_http_methods(['POST','GET','PATCH','DELETE'])
def workspace(request, slug=None):
    if request.method == 'POST':
        param = json.loads(request.body)
        if not param.get('title'):
            return JsonResponse({
                "message" : "Field missing",
                "status" : False
            })
        instance = WorkSpace.objects.create(**param)
        return JsonResponse({
            "message": "success",
            "status": True,
            "data": [instance.get_json()]
        })

    if request.method == "GET":
        data = []
        kwargs = {}
        if slug:
            if slug is not None and str(slug).isdigit():
                kwargs['id'] = slug
            elif slug is not None:
                kwargs['title'] = slug
        queryset = WorkSpace.objects.filter(**kwargs)
        if queryset.exists():
            for instance in queryset:
                data.append(instance.get_json())
            return JsonResponse({
                "message": "success",
                "status": True,
                "data": data
            })
        else:
            return JsonResponse({
                "message": "WorkSpace is Not Found.",
                "status": False,
            })

    if request.method == 'DELETE':
        param = json.loads(request.body)
        if not param.get('id'):
            return JsonResponse({
                "message": "ID Field missing" ,
                "status": False
            })
        WorkSpace.objects.filter(id = param['id']).delete()
        return JsonResponse({
            "message": "Workspace deleted successfully",
            "status": True
        })

    if request.method == 'PATCH':
        param = json.loads(request.body)
        if not param.get('id'):
            return JsonResponse({
                "message": "ID Field missing" ,
                "status": False
            })
        if not param.get('new_title'):
            return JsonResponse({
                "message": "New Title Field missing" ,
                "status": False
            })
        WorkSpace.objects.filter(id=int(param['id'])).update(title=param['new_title'])
        instance = WorkSpace.objects.get(title=param['new_title'])
        return JsonResponse({
            "message": "Workspace Updated successfully",
            "status": True ,
            "data": [instance.get_json()]
        })


@require_http_methods(["POST", "GET", "PATCH", "DELETE"])
def team(request, slug=None):
    if request.method == "POST":
        params = json.loads(request.body)
        users = params.pop('users')
        users = [User.objects.get(id=x) for x in users]
        params['workspace']=WorkSpace.objects.get(id=params['workspace'])
        instance = Team.objects.create(**params)
        instance.users.add(*users)
        return JsonResponse({
            "message": "success",
            "status": True,
            "data": [instance.get_json()]

        })

    if request.method == "GET":
        data = []
        kwargs = {}
        if slug:
            kwargs['id'] = slug
        queryset = Team.objects.filter(**kwargs)
        for instance in queryset:
            data.append(instance.get_json())
        return JsonResponse({
            "message": "success",
            "status": True,
            "data": data
        })

    if request.method == "DELETE":
        params = json.loads(request.body)
        instance = Team.objects.get(id=params.get("id"))
        Team.objects.filter(id=params.get("id")).delete()
        return JsonResponse({
            "message": "Delete Team successfully",
            "status": True,
            "data": [instance.get_json()]
        })

    if request.method == "PATCH":
        params = json.loads(request.body)
        workspace_instance = WorkSpace.objects.get(id=params['workspace'])
        Team.objects.filter(id=params.get('id')).update(workspace=workspace_instance)
        instance=Team.objects.get(id=params.get('id'))
        return JsonResponse({
            "message": " success",
            "status": True,
            "data": [instance.get_json()]
        })
'''
def workspace_with_param(request, slug):
    try:
        if slug is not None and slug.isdigit():
            instance = WorkSpace.objects.get(id=slug)
            if instance.id == int(slug):
                msg = f"Workspace with id {slug}"
                return JsonResponse({
                    "message": msg,
                    "status": True,
                    "data": [instance.get_json()]
                })
        elif slug is not None:
            instance = WorkSpace.objects.get(title=slug)
            if instance.title == slug:
                msg = f"Workspace with Title {slug}"
                return JsonResponse({
                    "message": msg,
                    "status": True,
                    "data": [instance.get_json()]
                })
    except WorkSpace.DoesNotExist:
        return JsonResponse({
                "message": "WorkSpace is Not Found.",
                "status": False,
        })       
'''