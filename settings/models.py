from django.db import models
from django.contrib.auth.models import User
from authentication.models import BaseClass
from authentication.compose import retrieve_user_info


class WorkSpace(BaseClass):
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title

    def get_json(self):
        return dict(
            id=self.id,
            title=self.title,
            created=self.created,
            modified=self.modified
        )

class Team(BaseClass):
    workspace = models.OneToOneField(WorkSpace, on_delete=models.CASCADE)
    users = models.ManyToManyField(User, blank=True)

    def __str__(self):
        return self.workspace.title

    def get_json(self):
        user_ids = [x.id for x in self.users.all()]
        return dict(
            id=self.id,
            workspace=self.workspace.get_json(),
            users=retrieve_user_info(user_ids),
            created=self.created,
            modified=self.modified,
        )


