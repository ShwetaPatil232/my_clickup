from django.contrib import admin
from .models import Employee, LeaveManagement


# Register your models here.


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['id','user', 'role']
    list_filter = ['id']
    search_fields = ['id']


@admin.register(LeaveManagement)
class LeaveManagementAdmin(admin.ModelAdmin):
    list_display = ['id', 'start_date', 'end_date', 'reason', 'status']
    list_filter = ['id']
    search_fields = ['id']