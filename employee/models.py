from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import datetime

# Create your models here.

class Employee(models.Model):

    LEAVE_STATUS = [(1, 'Sent for approval'), (2, 'Approved'), (3, 'Rejected')]

    ROLE_CHOICES = [(1, 'Support specialist'), (2, 'Computer programmer'),(3, 'Web developer'), (4, 'IT technician'),
                    (5, 'Systems analyst'), (6, 'Network engineer'), (7, 'Ux designer'), (8, 'Database administrator'),
                    (9,'Computer scientist'), (10, 'IT director'), (11, 'Software engineer'), (12, 'IT security specialist'),
                    (13,'Data scientist')]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    employee_id = models.CharField(max_length=20)
    role = models.IntegerField(choices=ROLE_CHOICES, default=11)
    join_date = models.DateField(auto_now_add=True)
    address = models.CharField(max_length=50)

    def __str__(self):
        return self.user

    def get_json(self):
        return dict(
            id=self.id,
            employee_id= self.employee_id,
            first_name=self.user.first_name,
            last_name=self.user.last_name,
            email=self.user.email,
            role= self.role,
            join_date=self.join_date,
            address=self.address,
        )


class LeaveManagement(models.Model):
    # Status of Application
    LEAVE_STATUS = [(1, 'Sent for approval'), (2, 'Approved'), (3, 'Rejected')]

    # Types of leave
    LEAVE_TYPES = ((1, 'Half Day'), (2, 'Full Day'),)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    leave_type = models.IntegerField(choices=LEAVE_TYPES, default=2)
    start_date = models.DateField(verbose_name='Start Date')
    end_date = models.DateField(verbose_name='End Date')
    reason = models.TextField(blank=True)
    status = models.IntegerField(choices=LEAVE_STATUS, default=1)
    is_approved = models.BooleanField(default=False)

    def get_json(self):
        return dict(
            id=self.id,
            user=self.user,
            reason=self.reason,
            leave_type=self.leave_type,
            start_date=self.start_date,
            end_date=self.end_date,
            status=str(self.status)
        )
