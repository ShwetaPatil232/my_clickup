import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Employee
from .models import LeaveManagement
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

@login_required
@require_http_methods(['POST', 'GET', 'PATCH', 'DELETE'])
def employee(request, slug = None):
    if request.method == 'POST':
        param = json.loads(request.body)
        if not param.get('first_name'):
            return JsonResponse({
                "message": "Employee name Field missing",
                "status": False
            })
        first_name = param.pop('first_name')
        last_name = param.pop('last_name')
        email = param.pop('email')
        param['user']= User.objects.create(first_name=first_name,last_name=last_name, email=email)
        instance = Employee.objects.create(**param)
        return JsonResponse({
            "message": "New Employee is Added",
            "status": True,
            "data": [instance.get_json()]
        })

    if request.method == 'GET':
        data = []
        if slug == None:
            queryset = Employee.objects.all()
            for instance in queryset:
                data.append(instance.get_json())
            return JsonResponse({
                "message": "success",
                "status": True,
                "data": data
            })
        elif slug is not None and slug.isdigit():
            instance = Employee.objects.get(Q(id__icontains=int(slug)))
            if instance.id == int(slug):
                return JsonResponse({
                    "message": "success",
                    "status": True,
                    "data": [instance.get_json()]})
            queryset: Employee = Employee.objects.filter(
            Q(first_name__icontains=slug) | Q(last_name__icontains=slug) | Q(role__icontains=slug))
            if queryset.exists():
                for instance in queryset:
                    data.append([instance.get_json()])
                return JsonResponse({
                "message": "success",
                "status": True,
                "data": data})
            else:
                return JsonResponse({
                "message": "Detail not found",
                "status": False })

    if request.method == 'DELETE':
        param = json.loads(request.body)
        if not param.get('id'):
            return JsonResponse({
                "message": "Employee id Field missing",
                "status": False
            })
        Employee.objects.filter(id=param['id']).delete()
        return JsonResponse({
            "message": "Employee deleted successfully",
            "status": True
        })

    if request.method == 'PATCH':
        param = json.loads(request.body)
        if not param.get('id'):
            return JsonResponse({
                "message": "Employee id Field missing",
                "status": False
            })
        Employee.objects.filter(id=param['id']).update(**param)
        instance = Employee.objects.get(id=param['id'])
        return JsonResponse({
            "message": "Employee Details Updated successfully",
            "status": True,
            "data": [instance.get_json()]
        })

@login_required
@require_http_methods(['POST', 'GET', 'PATCH', 'DELETE'])
def leave(request):
    if request.method == 'POST':
        param = json.loads(request.body)
        user = User.objects.get(id=param.get('user'))
        param['user'] = user
        if (param.get('user') == None) | (param.get('reason') == None) | (param.get('start_date')==None) | (param.get('end_date') == None):
            return JsonResponse({
                "message": "Field missing",
                "status": False
            })
        instance = LeaveManagement.objects.create(**param)
        return JsonResponse({
            "message": "Leave Request is Added",
            "status": True,
            "data": [instance.get_json()]
        })

    if request.method == 'GET':
        data = []
        queryset = LeaveManagement.objects.all()
        for instance in queryset:
            data.append(instance.get_json())
        return JsonResponse({
            "message": "success",
            "status": True,
            "data": data
        })

    if request.method == 'DELETE':
        param = json.loads(request.body)
        if not param.get('id'):
            return JsonResponse({
                "message": "Leave id Field missing",
                "status": False
            })
        LeaveManagement.objects.filter(id=param['id']).delete()
        return JsonResponse({
            "message": "Leave Request is Deleted successfully",
            "status": True
        })

    if request.method == 'PATCH':
        param = json.loads(request.body)
        if not param.get('id'):
            return JsonResponse({
                "message": "Leave ID Field missing",
                "status": False
            })
        LeaveManagement.objects.filter(id=param['id']).update(**param)
        instance = LeaveManagement.objects.get(id=param['id'])
        return JsonResponse({
            "message": "Leave Details Updated successfully",
            "status": True,
            "data": [instance.get_json()]
        })

'''
def employee_search_filters(request , slug=None):
    data = []
    if slug is not None and slug.isdigit():
        instance = Employee.objects.filter( Q(id__icontains=int(slug)))
        if instance.id == int(slug):
            return JsonResponse({
                "message": "success",
                "status": True,
                "data": [instance.get_json()]})

    queryset: Employee = Employee.objects.filter( Q(first_name__icontains=slug) | Q(last_name__icontains=slug) | Q(role__icontains=slug))
    if queryset.exists():
        for instance in queryset:
            data.append([instance.get_json()])
        return JsonResponse({
            "message": "success",
            "status": True,
            "data": data
        })
    else:
        return JsonResponse({
            "message": "Detail not found",
            "status": False
        })
'''